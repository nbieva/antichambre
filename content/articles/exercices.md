---
title: Quelques idées d'exercices
description: Quelques idées d'exercices
---

# Quelques idées d'exercices

Essayez d'en choisir l'un ou l'autre en fonction du temps que nous avons. [Collez l'adresse de partage de votre sketch](/share) dans notre fichier partagé.

Vous pouvez chaque fois partir de la base qui vous est proposée, et la personnaliser

# Un outil de dessin

+  Ouvrez l'éditeur de P5*js.
+ **Collez**-y le code ci-dessous.
+ **Sauvez** votre document (après vous être connecté)
+ **Exécutez** votre code en appuyant sur play.
+ **Augmentez**-le, modifiez-le petit à petit.
+ **Sauvez** régulièrement votre travail (Cmd+S)
+ La taille de votre forme doit doubler lorsque vous cliquez sur la souris.

```javascript
void setup() {
    // dimensions du canvas
    size(800,600);
    // fond blanc
    background(255);
    rectMode(CENTER); //Les rectangles seront tracés à partir de leur centre
}

void draw(){
    // on dessine une ellipse dont les positions x et y correspondent à la position de la souris.
    rect(mouseX, mouseY, 20, 20);
}

// On exporte l'image quand on appuie sur une touche
void keyPressed() {
    save("monDessin-"+day()+"-"+hour()+"-"+minute()+"-"+second()+".jpg");
}
```

# Formes simples

+ Familiarisez-vous avec l'espace de votre sketch en placant différentes formes de couleurs différentes.
+ [https://codeb3.netlify.app/exercices/formes-simples.html](https://codeb3.netlify.app/exercices/formes-simples.html)

# Créez un générateur de formes aléatoires

+ Créez un [générateur de formes aléatoires](https://editor.p5js.org/Antichambre/sketches/n1SHjv5Fa) à l'aide de la fonction random() et des fonctions beginShape(), vertex() et endShape().
+ Changez de couleur à chaque boucle. Vous pouvez également modifier l'épaisseur et la couleur du contour.
+ [https://codeb3.netlify.app/exercices/generatif.html](https://codeb3.netlify.app/exercices/generatif.html)

```javascript
let x1, y1, x2, y2, x3, y3, x4, y4;

function setup() {
  createCanvas(400, 400);
  frameRate(2);
  noStroke();
}

function draw() {
  background(120);
  
  // On calcule les variables
  x1 = random(100, 200);
  y1 = random(100, 200);
  x2 = random(200, width - 100);
  y2 = random(100, 200);
  x3 = random(200, width - 100);
  y3 = random(200, height - 100);
  x4 = random(100, 200);
  y4 = random(200, height - 100);

  // On dessine la forme
  beginShape();
    vertex(x1, y1);
    vertex(x2, y2);
    vertex(x3, y3);
    vertex(x4, y4);
  endShape(CLOSE);
}
```

# Damien Hirst

+ Deux propositions à partir du travail de Damien Hirst
+ [Mickey](http://www.damienhirst.com/mickey) et [Spots](https://www.google.com/search?q=damien+hirst+spots&tbm=isch&ved=2ahUKEwjP4J2IzJjuAhUH16QKHfVlBBwQ2-cCegQIABAA&oq=damien+hirst+spots&gs_lcp=CgNpbWcQAzIECAAQEzoECAAQQzoCCAA6BAgAEB46BggAEAgQHlCzygFYqtEBYLHTAWgAcAB4AIABOIgBgQKSAQE1mAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=G73-X4_nCIeukwX1y5HgAQ&bih=1338&biw=1269)
+ Tracé de formes simples, animation, éventuellement conditions
+ Animez Mickey


```javascript
function setup() {
  createCanvas(350, 550);
  noStroke();

  // DISQUES ROUGES
  fill(170, 0, 20);
  ellipse(175, 205, 37, 37);
  ellipse(175, 205, 37, 37);
  ellipse(231, 290, 138, 138);

  // DISQUES NOIRS
  fill(0);
  ellipse(150, 47.5, 95, 95);
  ellipse(75, 120, 95, 95);
  ellipse(202, 130, 30, 30);
  ellipse(240, 130, 30, 30);
  ellipse(278, 154, 42, 42);
  ellipse(232, 290, 30, 30);
  ellipse(265, 265, 30, 30);

  // DISQUES JAUNES
  fill(220, 200, 0);
  ellipse(66, 415, 132, 132);
  ellipse(267, 450, 132, 132);
}
```

# [Créez une Horloge](https://codeb3.netlify.app/exercices/horloge.html)

+ [https://codeb3.netlify.app/exercices/horloge.html](https://codeb3.netlify.app/exercices/horloge.html)
+ Créez une façon inédite de visualiser le temps
+ Créez une alarme. Votre horloge pourrait "sonner" à une heure donnée.


```javascript
function setup() {
  createCanvas(windowWidth,windowHeight); // mais pourrait aussi être 800,600
  noStroke(); // supprime les contours
}

function draw() {
  background(0, 122, 142);
  fill(76, 165, 71);
  rect(0,0,hour()*(width/24),height/3);
  fill(61, 121, 87);
  rect(0,height/3,minute()*(width/60),height/3);
  fill(64, 94, 104);
  rect(0,2*(height/3),second()*(width/60),height/3);
}
```

# Dessiner avec une image

+ Remplacez l'image par une image de votre choix.
+ Expérimentez
+ Notez qu'il faut uploader l'image dans l'éditeur

```javascript
let img;

function preload() {
  img = loadImage('images/sunset.jpg');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
}

function draw() {
  if(mouseIsPressed) {
    image(img, mouseX, mouseY, mouseY/3, mouseY/5);
  }
}
```

# [10 Print](https://10print.org/)

+ Créez votre propre variation de ce petit programme.
+ Modifiez sa taille, les couleurs, l'épaisseur des contours
+ Modifiez certains paramètres en fonction de la position de la souris.


```javascript
let x = 0;
let y = 0;
let cote = 10;

function setup() {
  createCanvas(200, 200);
  background(0);
}

function draw() {
  stroke(255);
  if (random(1) < 0.5) {
    line(x, y, x + cote, y + cote);
  } else {
    line(x, y + cote, x + cote, y);
  }
  x = x + cote;
  if (x > width) {
    x = 0;
    y = y + cote;
  }
}
```
