---
title: Quelques snippets
description: Quelques snippets
---

# Quelques bouts de code..

## Structure d'un programme

```javascript
function setup() {
    // Code qui se jouera UNE SEULE fois
    createCanvas(600,400);
    noStroke();
}

function draw() {
    //Code, qui se jouera EN BOUCLE, sauf instruction contraire.
    ellipse(mouseX, mouseY, 20, 20);
}
```
<!--
<iframe class="p5sketch" src="https://editor.p5js.org/Antichambre/embed/VCyMymUW2"></iframe>-->

## Les variables

```javascript
// On déclare une variable
var monNombre;

function setup() {
    createCanvas(500,500);
    // On lui assigne une valeur
    monNombre = 100;
}
function draw() {
    // On l'utilise
    ellipse(monNombre, monNombre, monNombre/2,  monNombre/2); // Dessinera un cercle de 50 sur 50 positionné en 100,100
    // On la fait évoluer (l'éllipse va se déplacer et grandir)
    monNombre = monNombre+2;
}
```

## Les conditions

```javascript
function setup() {
    createCanvas(500,500);
}
function draw() {
    if(mouseIsPressed) {
        fill(255,255,0);
    } else {
        fill(255);
    }
    ellipse(mouseX, mouseY, 80, 80;
}
```

## Déplacer un rectangle dans tout le sketch

```javascript
let posx = 0;
let posy = 0;

function setup() {
    createCanvas(500,500);
    noStroke();
}
function draw() {
    fill(random(0,255));
    if(posx >= width) {
        posx = 0;
        posy = posy + 50;
    } 
    rect(posx, posy, 50, 50);
    posx = posx +50;
}
```

## Exporter le canvas

Les lignes suivantes exporteront le canvas chaque fois que l'on appuiera sur une touche.
Ce bloc de code se met à la fin du programme, au même niveau que setup() et draw().

```javascript
function keyPressed() {
    // À chaque pression sur une touche, l'image sera sauvegardée
    save("monDessin-"+day()+"-"+hour()+"-"+minute()+"-"+second()+".jpg");
}
```