---
title: Partager votre travail
description: Partager votre travail
---

# Partager votre sketch

Editeur: [https://editor.p5js.org/](https://editor.p5js.org/)

## Création de compte et sauvegardes

[Créer un compte](https://editor.p5js.org/signup) (en haut à droite) vous permettra de sauver et de partager vos sketches directement depuis l'interface de l'éditeur.

Vous pouvez créer autant de sketches que vous désirez. Veillez à noter chaque URL de partage (Present) dans [cette feuille de calcul sur le Drive](https://docs.google.com/spreadsheets/d/1nEs-s-N6C0LOMSAud2qOxOkmcWtu04rYwwVnGGde3vA/edit?usp=sharing).

Une fois sauvé une première fois, votre sketch sera sauvé en continu automatiquement. Le raccourci **Cmd+S** forcera la sauvegarde manuellement.

<a data-fancybox class="fancy" title="P5js" href="mep-save.png">![P5js](mep-save.png)</a>
<span class="legende">Sauvez votre sketch (File > save)</span>

Vous pouvez également renommer votre sketch. C'est bien entendu conseillé si vous voulez vous y retrouver...

<a data-fancybox class="fancy" title="P5js" href="mep-rename.png">![P5js](mep-rename.png)</a>
<span class="legende">Renommez votre sketch (Donnez lui un titre)</span>

Une fois le sketch sauvegardé, vous aurez accès à la fonction de partage (Share). Plusieurs options s'offrent à vous.

<a data-fancybox class="fancy" title="P5js" href="mep-share.png">![P5js](mep-share.png)</a>

<a data-fancybox class="fancy" title="P5js" href="mep-present.png">![P5js](mep-present.png)</a>
<span class="legende">Partagez votre sketch.</span>


Pour vos rendus, copiez l'URL de présentation (Present) dans [notre fichier partagé](https://docs.google.com/spreadsheets/d/1nEs-s-N6C0LOMSAud2qOxOkmcWtu04rYwwVnGGde3vA/edit?usp=sharing). Cela vous permettra d'avoir une vue sur les réalisations des autres étudiants également.

<a data-fancybox class="fancy" title="P5js" href="mep-sheet.png">![P5js](mep-sheet.png)</a>
<span class="legende">Partagez votre sketch.</span>
