export  const  state  = () => ({
    darkMode: false,
    boxname: "Antichambre"
});
export  const  mutations  = {
  TOGGLE_THEME: state => {
      state.darkMode = !state.darkMode
  }
};
export  const  getters  = {
    themeStatus: state => {
      return state.darkMode
    },
    boxName: state => {
      return state.boxname
    }
};
/*
Heliotrope
American rose
Bittersweet
#fe6f5e
Carmine red (crayon que j'avais reçu)
#ff0038
Coral red
#ff4040
Deep peach
#ffcba4
Light green
#90ee90
Outer Space
#414a4c
*/